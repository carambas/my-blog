import { Component, Input, OnInit } from '@angular/core';
import { Post } from '../post';

@Component({
  selector: 'app-postlistitem',
  templateUrl: './postlistitem.component.html',
  styleUrls: ['./postlistitem.component.scss']
})
export class PostlistitemComponent implements OnInit {
  @Input() post: Post;

  constructor() { }

  ngOnInit() {
  }

  onLoveIt() {
    this.post.loveIts++;
    console.log('onDontLoveIt: loveIts=' + this.post.loveIts);
  }

  onDontLoveIt() {
    this.post.loveIts--;
    console.log('onDontLoveIt: loveIts=' + this.post.loveIts);
  }

}
